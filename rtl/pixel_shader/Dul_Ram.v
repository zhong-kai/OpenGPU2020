//__________________________________________________________________
//
//  Module      :   Dul_Ram
//              
//  By          :   Kejie
//  E-mail      :   Kejie1208@126.com
//  Created     :   08/12/10 
//___________________________________________________________________

module Dul_Ram  #(
  
parameter   L=8,
parameter   DW=6
)
(
  input                     rst_n,
  input                     ac,
  input       [L-1:0]       aa,
  input       [DW-1:0]      ad,
  input                     aw,
  
  input                     rst_nb,
  input                     bc,
  input       [L-1:0]       ba,
  output reg  [DW-1:0]      bd,
  input                     br,
  // SCAN pins
  input                     SCAN_mode,
  output      [L-1:0]       obs_aa,
//  output      [DW-1:0]      obs_ad,
  output                    obs_aw,  
  output      [L-1:0]       obs_ba,
  output                    obs_br 
);

`ifdef ASIC


wire              clkb;
wire [L-1:0]      ab_in;
wire [DW-1:0]     db_in;
wire              cenb;

assign            clkb = ac;
assign #1         ab_in = aa;
assign #1         db_in = ad;
assign #1         cenb = ~aw;

wire              clka;
wire [L-1:0]      aa_in;

wire              cena;

assign            clka = bc;
assign #1         aa_in = ba;
assign #1         cena = ~br;



generate
/*if(DW == 32 && L == 5)
begin
wire [DW-1:0]     qa_in;
always @* bd = SCAN_mode ? ad : qa_in;
    dpram_32x32 Dul_Ram_uut(
    .CLKB(clkb),
    .AB(ab_in),
    .DB(db_in),
    .CENB(cenb),
    .CLKA(clka),
    .AA(aa_in),
    .QA(qa_in),
    .CENA(cena)
    );
end
else
if(DW == 64 && L == 5)
begin
wire [DW-1:0]     qa_in;
always @* bd = SCAN_mode ? ad : qa_in;
    dpram_32x64 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);
end
else if(DW == 39 && L == 5)
begin
wire [DW-1:0]     qa_in;
always @* bd = SCAN_mode ? ad : qa_in;
    dpram_32x39 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);
end
else */
if(DW == 72 && L == 4)
begin
wire [DW-1:0]qa_in;
wire [DW-1:0]bd_wire;
always @* bd = bd_wire;
MX2X8MTR u_obs_dq[DW-1:0] (.S0(SCAN_mode), .A(qa_in), .B(ad), .Y(bd_wire));    
    
    dpram_16x72 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);          
SDFFRQX1MTR  u_obs_aa[L-1:0]  ( .D(aa), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aa) );  
//SDFFRQX1MTR  u_obs_ad[DW-1:0] ( .D(ad), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_ad) );  
SDFFRQX1MTR  u_obs_aw         ( .D(cenb), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aw) );  

SDFFRQX1MTR  u_obs_ba[L-1:0]  ( .D(ba), .SI(1'b0), .SE(1'b0), .CK(bc), .RN(rst_nb), .Q(obs_ba) );  
SDFFRQX1MTR  u_obs_br         ( .D(cena), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_nb), .Q(obs_br) );  
                                                                      
end     
else if(DW == 32 && L == 6)
begin
wire [DW-1:0]qa_in;
wire [DW-1:0]bd_wire;
always @* bd = bd_wire;
MX2X8MTR u_obs_dq[DW-1:0] (.S0(SCAN_mode), .A(qa_in), .B(ad), .Y(bd_wire));    


    dpram_64x32 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);
SDFFRQX1MTR  u_obs_aa[L-1:0]  ( .D(aa), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aa) );  
//SDFFRQX1MTR  u_obs_ad[DW-1:0] ( .D(ad), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_ad) );  
SDFFRQX1MTR  u_obs_aw         ( .D(cenb), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aw) );  

SDFFRQX1MTR  u_obs_ba[L-1:0]  ( .D(ba), .SI(1'b0), .SE(1'b0), .CK(bc), .RN(rst_nb), .Q(obs_ba) );  
SDFFRQX1MTR  u_obs_br         ( .D(cena), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_nb), .Q(obs_br) ); 
end
else if(DW == 7 && L == 6)
begin
wire [DW-1:0]qa_in;
wire [DW-1:0]bd_wire;
always @* bd = bd_wire;
MX2X8MTR u_obs_dq[DW-1:0] (.S0(SCAN_mode), .A(qa_in), .B(ad), .Y(bd_wire));    
    
    dpram_64x7 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);
SDFFRQX1MTR  u_obs_aa[L-1:0]  ( .D(aa), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aa) );  
//SDFFRQX1MTR  u_obs_ad[DW-1:0] ( .D(ad), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_ad) );  
SDFFRQX1MTR  u_obs_aw         ( .D(cenb), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aw) );  

SDFFRQX1MTR  u_obs_ba[L-1:0]  ( .D(ba), .SI(1'b0), .SE(1'b0), .CK(bc), .RN(rst_nb), .Q(obs_ba) );  
SDFFRQX1MTR  u_obs_br         ( .D(cena), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_nb), .Q(obs_br) ); 

end   

else if(DW == 13 && L == 6)
begin
wire [DW-1:0]qa_in;
wire [DW-1:0]bd_wire;
always @* bd = bd_wire;
MX2X8MTR u_obs_dq[DW-1:0] (.S0(SCAN_mode), .A(qa_in), .B(ad), .Y(bd_wire));    
    
    dpram_64x13 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);
SDFFRQX1MTR  u_obs_aa[L-1:0]  ( .D(aa), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aa) );  
//SDFFRQX1MTR  u_obs_ad[DW-1:0] ( .D(ad), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_ad) );  
SDFFRQX1MTR  u_obs_aw         ( .D(cenb), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aw) );  

SDFFRQX1MTR  u_obs_ba[L-1:0]  ( .D(ba), .SI(1'b0), .SE(1'b0), .CK(bc), .RN(rst_nb), .Q(obs_ba) );  
SDFFRQX1MTR  u_obs_br         ( .D(cena), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_nb), .Q(obs_br) ); 

end 

else if(DW == 13 && L == 7)
begin
wire [DW-1:0]qa_in;
wire [DW-1:0]bd_wire;
always @* bd = bd_wire;
MX2X8MTR u_obs_dq[DW-1:0] (.S0(SCAN_mode), .A(qa_in), .B(ad), .Y(bd_wire));    
    
    dpram_128x13 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);
SDFFRQX1MTR  u_obs_aa[L-1:0]  ( .D(aa), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aa) );  
//SDFFRQX1MTR  u_obs_ad[DW-1:0] ( .D(ad), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_ad) );  
SDFFRQX1MTR  u_obs_aw         ( .D(cenb), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aw) );  

SDFFRQX1MTR  u_obs_ba[L-1:0]  ( .D(ba), .SI(1'b0), .SE(1'b0), .CK(bc), .RN(rst_nb), .Q(obs_ba) );  
SDFFRQX1MTR  u_obs_br         ( .D(cena), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_nb), .Q(obs_br) ); 

end 
else if(DW == 36 && L == 7)
begin
wire [DW-1:0]qa_in;
wire [DW-1:0]bd_wire;
always @* bd = bd_wire;
MX2X8MTR u_obs_dq[DW-1:0] (.S0(SCAN_mode), .A(qa_in), .B(ad), .Y(bd_wire));    
    
    dpram_128x36 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);
SDFFRQX1MTR  u_obs_aa[L-1:0]  ( .D(aa), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aa) );  
//SDFFRQX1MTR  u_obs_ad[DW-1:0] ( .D(ad), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_ad) );  
SDFFRQX1MTR  u_obs_aw         ( .D(cenb), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aw) );  

SDFFRQX1MTR  u_obs_ba[L-1:0]  ( .D(ba), .SI(1'b0), .SE(1'b0), .CK(bc), .RN(rst_nb), .Q(obs_ba) );  
SDFFRQX1MTR  u_obs_br         ( .D(cena), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_nb), .Q(obs_br) ); 

end 
else if(DW == 256 && L == 3 )
begin
wire [DW-1:0]qa_in;
wire [DW-1:0]bd_wire;
always @* bd = bd_wire;
MX2X8MTR u_obs_dq[DW-1:0] (.S0(SCAN_mode), .A(qa_in), .B(ad), .Y(bd_wire));    
    
    dpram_8x128 Dul_Ram_uut0(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in[127:0]),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in[127:0]),
.CENA(cena)
);
    dpram_8x128 Dul_Ram_uut1(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in[255:128]),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in[255:128]),
.CENA(cena)
); 
SDFFRQX1MTR  u_obs_aa[L-1:0]  ( .D(aa), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aa) );  
//SDFFRQX1MTR  u_obs_ad[DW-1:0] ( .D(ad), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_ad) );  
SDFFRQX1MTR  u_obs_aw         ( .D(cenb), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aw) );  

SDFFRQX1MTR  u_obs_ba[L-1:0]  ( .D(ba), .SI(1'b0), .SE(1'b0), .CK(bc), .RN(rst_nb), .Q(obs_ba) );  
SDFFRQX1MTR  u_obs_br         ( .D(cena), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_nb), .Q(obs_br) ); 

end  
else if(DW == 128 && L == 5)
begin
wire [DW-1:0]qa_in;
wire [DW-1:0]bd_wire;
always @* bd = bd_wire;
MX2X8MTR u_obs_dq[DW-1:0] (.S0(SCAN_mode), .A(qa_in), .B(ad), .Y(bd_wire));    
    
    dpram_32x128 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);
SDFFRQX1MTR  u_obs_aa[L-1:0]  ( .D(aa), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aa) );  
//SDFFRQX1MTR  u_obs_ad[DW-1:0] ( .D(ad), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_ad) );  
SDFFRQX1MTR  u_obs_aw         ( .D(cenb), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aw) );  

SDFFRQX1MTR  u_obs_ba[L-1:0]  ( .D(ba), .SI(1'b0), .SE(1'b0), .CK(bc), .RN(rst_nb), .Q(obs_ba) );  
SDFFRQX1MTR  u_obs_br         ( .D(cna), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_nb), .Q(obs_br) ); 

end
else if(DW == 38 && L == 6)
begin
wire [DW-1:0]qa_in;
wire [DW-1:0]bd_wire;
always @* bd = bd_wire;
MX2X8MTR u_obs_dq[DW-1:0] (.S0(SCAN_mode), .A(qa_in), .B(ad), .Y(bd_wire));    
    
    dpram_64x38 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);
SDFFRQX1MTR  u_obs_aa[L-1:0]  ( .D(aa), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aa) );  
//SDFFRQX1MTR  u_obs_ad[DW-1:0] ( .D(ad), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_ad) );  
SDFFRQX1MTR  u_obs_aw         ( .D(cenb), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aw) );  

SDFFRQX1MTR  u_obs_ba[L-1:0]  ( .D(ba), .SI(1'b0), .SE(1'b0), .CK(bc), .RN(rst_nb), .Q(obs_ba) );  
SDFFRQX1MTR  u_obs_br         ( .D(cena), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_nb), .Q(obs_br) ); 
end     
else if(DW == 64 && L == 6)
begin
wire [DW-1:0]qa_in;
wire [DW-1:0]bd_wire;
always @* bd = bd_wire;
MX2X8MTR u_obs_dq[DW-1:0] (.S0(SCAN_mode), .A(qa_in), .B(ad), .Y(bd_wire));    
    
    dpram_64x64 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);
SDFFRQX1MTR  u_obs_aa[L-1:0]  ( .D(aa), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aa) );  
//SDFFRQX1MTR  u_obs_ad[DW-1:0] ( .D(ad), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_ad) );  
SDFFRQX1MTR  u_obs_aw         ( .D(cenb), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aw) );  

SDFFRQX1MTR  u_obs_ba[L-1:0]  ( .D(ba), .SI(1'b0), .SE(1'b0), .CK(bc), .RN(rst_nb), .Q(obs_ba) );  
SDFFRQX1MTR  u_obs_br         ( .D(cena), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_nb), .Q(obs_br) ); 
end
else if(DW == 68 && L == 6)
begin
wire [DW-1:0]qa_in;
wire [DW-1:0]bd_wire;
always @* bd = bd_wire;
MX2X8MTR u_obs_dq[DW-1:0] (.S0(SCAN_mode), .A(qa_in), .B(ad), .Y(bd_wire));    
    
    dpram_64x68 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);
SDFFRQX1MTR  u_obs_aa[L-1:0]  ( .D(aa), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aa) );  
//SDFFRQX1MTR  u_obs_ad[DW-1:0] ( .D(ad), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_ad) );  
SDFFRQX1MTR  u_obs_aw         ( .D(cenb), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aw) );  

SDFFRQX1MTR  u_obs_ba[L-1:0]  ( .D(ba), .SI(1'b0), .SE(1'b0), .CK(bc), .RN(rst_nb), .Q(obs_ba) );  
SDFFRQX1MTR  u_obs_br         ( .D(cena), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_nb), .Q(obs_br) ); 
end     
else if(DW == 32 && L == 7)
begin
wire [DW-1:0]qa_in;
wire [DW-1:0]bd_wire;
always @* bd = bd_wire;
MX2X8MTR u_obs_dq[DW-1:0] (.S0(SCAN_mode), .A(qa_in), .B(ad), .Y(bd_wire));    
    
    dpram_128x32 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);
SDFFRQX1MTR  u_obs_aa[L-1:0]  ( .D(aa), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aa) );  
//SDFFRQX1MTR  u_obs_ad[DW-1:0] ( .D(ad), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_ad) );  
SDFFRQX1MTR  u_obs_aw         ( .D(cenb), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aw) );  

SDFFRQX1MTR  u_obs_ba[L-1:0]  ( .D(ba), .SI(1'b0), .SE(1'b0), .CK(bc), .RN(rst_nb), .Q(obs_ba) );  
SDFFRQX1MTR  u_obs_br         ( .D(cena), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_nb), .Q(obs_br) ); 
end
else if(DW == 39 && L == 7)
begin
wire [DW-1:0]qa_in;
wire [DW-1:0]bd_wire;
always @* bd = bd_wire;
MX2X8MTR u_obs_dq[DW-1:0] (.S0(SCAN_mode), .A(qa_in), .B(ad), .Y(bd_wire));    
    
    dpram_128x39 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);
SDFFRQX1MTR  u_obs_aa[L-1:0]  ( .D(aa), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aa) );  
//SDFFRQX1MTR  u_obs_ad[DW-1:0] ( .D(ad), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_ad) );  
SDFFRQX1MTR  u_obs_aw         ( .D(cenb), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aw) );  

SDFFRQX1MTR  u_obs_ba[L-1:0]  ( .D(ba), .SI(1'b0), .SE(1'b0), .CK(bc), .RN(rst_nb), .Q(obs_ba) );  
SDFFRQX1MTR  u_obs_br         ( .D(cena), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_nb), .Q(obs_br) ); 
end  
else if(DW == 64 && L == 7)
begin
wire [DW-1:0]qa_in;
wire [DW-1:0]bd_wire;
always @* bd = bd_wire;
MX2X8MTR u_obs_dq[DW-1:0] (.S0(SCAN_mode), .A(qa_in), .B(ad), .Y(bd_wire));    
    
    dpram_128x64 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);
SDFFRQX1MTR  u_obs_aa[L-1:0]  ( .D(aa), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aa) );  
//SDFFRQX1MTR  u_obs_ad[DW-1:0] ( .D(ad), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_ad) );  
SDFFRQX1MTR  u_obs_aw         ( .D(cenb), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aw) );  

SDFFRQX1MTR  u_obs_ba[L-1:0]  ( .D(ba), .SI(1'b0), .SE(1'b0), .CK(bc), .RN(rst_nb), .Q(obs_ba) );  
SDFFRQX1MTR  u_obs_br         ( .D(cena), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_nb), .Q(obs_br) ); 
end  /*
else if(DW == 11 && L == 8)
begin
wire [DW-1:0]     qa_in;
always @* bd = SCAN_mode ? ad : qa_in; 
    dpram_256x11 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);
end   */
else if(DW == 32 && L == 8)
begin
wire [DW-1:0]qa_in;
wire [DW-1:0]bd_wire;
always @* bd = bd_wire;
MX2X8MTR u_obs_dq[DW-1:0] (.S0(SCAN_mode), .A(ad), .B(qa_in), .Y(bd_wire));   
    
    dpram_256x32 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);
SDFFRQX1MTR  u_obs_aa[L-1:0]  ( .D(aa), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aa) );  
//SDFFRQX1MTR  u_obs_ad[DW-1:0] ( .D(ad), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_ad) );  
SDFFRQX1MTR  u_obs_aw         ( .D(cenb), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aw) );  

SDFFRQX1MTR  u_obs_ba[L-1:0]  ( .D(ba), .SI(1'b0), .SE(1'b0), .CK(bc), .RN(rst_nb), .Q(obs_ba) );  
SDFFRQX1MTR  u_obs_br         ( .D(cena), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_nb), .Q(obs_br) ); 
end        
else if(DW == 8 && L == 8)
begin
wire [DW-1:0]qa_in;
wire [DW-1:0]bd_wire;
always @* bd = bd_wire;
MX2X8MTR u_obs_dq[DW-1:0] (.S0(SCAN_mode), .A(qa_in), .B(ad), .Y(bd_wire));    
    
    dpram_256x8 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);
SDFFRQX1MTR  u_obs_aa[L-1:0]  ( .D(aa), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aa) );  
//SDFFRQX1MTR  u_obs_ad[DW-1:0] ( .D(ad), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_ad) );  
SDFFRQX1MTR  u_obs_aw         ( .D(cenb), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aw) );  

SDFFRQX1MTR  u_obs_ba[L-1:0]  ( .D(ba), .SI(1'b0), .SE(1'b0), .CK(bc), .RN(rst_nb), .Q(obs_ba) );  
SDFFRQX1MTR  u_obs_br         ( .D(cena), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_nb), .Q(obs_br) ); 
end       
//else if(DW == 7 && L == 8)
//begin
//wire [DW-1:0]     qa_in;
//always @* bd = SCAN_mode ? ad : qa_in; 
//    dpram_256x7 Dul_Ram_uut(
//.CLKB(clkb),
//.AB(ab_in),
//.DB(db_in),
//.CENB(cenb),
//.CLKA(clka),
//.AA(aa_in),
//.QA(qa_in),
//.CENA(cena)
//);
//end
else if(DW == 64 && L == 8)
begin
wire [DW-1:0]qa_in;
wire [DW-1:0]bd_wire;
always @* bd = bd_wire;
MX2X8MTR u_obs_dq[DW-1:0] (.S0(SCAN_mode), .A(qa_in), .B(ad), .Y(bd_wire));    
    
    dpram_256x64 Dul_Ram_uut(
.CLKB(clkb),
.AB(ab_in),
.DB(db_in),
.CENB(cenb),
.CLKA(clka),
.AA(aa_in),
.QA(qa_in),
.CENA(cena)
);     
SDFFRQX1MTR  u_obs_aa[L-1:0]  ( .D(aa), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aa) );  
//SDFFRQX1MTR  u_obs_ad[DW-1:0] ( .D(ad), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_ad) );  
SDFFRQX1MTR  u_obs_aw         ( .D(cenb), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_n), .Q(obs_aw) );  

SDFFRQX1MTR  u_obs_ba[L-1:0]  ( .D(ba), .SI(1'b0), .SE(1'b0), .CK(bc), .RN(rst_nb), .Q(obs_ba) );  
SDFFRQX1MTR  u_obs_br         ( .D(cena), .SI(1'b0), .SE(1'b0), .CK(ac), .RN(rst_nb), .Q(obs_br) ); 
end
//else if(DW == 64 && L == 3)
//begin
//wire [DW-1:0]     qa_in;
//always @* bd = SCAN_mode ? ad : qa_in; 
//    dpram_8x64 Dul_Ram_uut(
//.CLKB(clkb),
//.AB(ab_in),
//.DB(db_in),
//.CENB(cenb),
//.CLKA(clka),
//.AA(aa_in),
//.QA(qa_in),
//.CENA(cena)
//);     
//end 


else
begin

reg   [DW-1:0] Ram[(1<<L)-1:0];

integer k;

always@(posedge ac or negedge rst_n)
begin
    if(!rst_n)
        begin
            for(k=0;k<(1<<L);k=k+1)
               Ram[k] <=   {DW{1'b0}};
        end
    else if(aw)  Ram[aa]<=  ad;
end

always@(posedge bc or negedge rst_nb)
begin
    if(!rst_nb) bd <=  {DW{1'b0}};  
    else if(br)  bd<=  Ram[ba];
end

end
endgenerate
`else
reg   [DW-1:0] Ram[(1<<L)-1:0]/* synthesis syn_ramstyle="block_ram " */;

always@(posedge ac)
if(aw)  Ram[aa]<=  ad;

always@(posedge bc)
if(br)  bd<=  Ram[ba];
`endif



endmodule
