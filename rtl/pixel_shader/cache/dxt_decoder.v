

module dxt_decoder(
        input                   clk,
        input                   rst_n,
        input                   rd_en_ff2,
        input                   cache_busy,
    input       [63:0]      l1_dout,
    output  reg [23:0]      dxt_texel,
        input       [3:0]       in_texture_addr_ff2
);

reg  [7:0]    pixel_tag;


//texel = {texel_h,texel_l}
//-------------------------------
//|Word address | 16-bit Word   |
//+-------------+---------------+
//|    0        |  Color0       |
//|    1        |  Color1       |
//|    2        |  Bitmap Word0 |
//|    3        |  Bitmap Word1 |
//-------------------------------

//for color0(texel_l[15:0]) and color1(texel_l[31:16])
//----------------
//|Bits  |Color  |
//+------+-------+
//|4:0   |Bule   |
//|10:5  |Green  |
//|15:11 |RED    |
//----------------


//for texel_h
//-----------------------
//|Bits   |     Texel   |
//+-------+-------------+
//|1:0    | Texel[0][0] |
//|3:2    | Texel[0][1] |
//|5:4    | Texel[0][2] |
//|7:6    | Texel[0][3] |
//|9:8    | Texel[1][0] |
//|11:10  | Texel[1][1] |
//|13:12  | Texel[1][2] |
//|15:14  | Texel[1][3] |
//|17:16  | Texel[2][0] |
//|19:18  | Texel[2][1] |
//|21:20  | Texel[2][2] |
//|23:22  | Texel[2][3] |
//|25:24  | Texel[3][0] |
//|27:26  | Texel[3][1] |
//|29:28  | Texel[3][2] |
//|31:30  | Texel[3][3] |
//-----------------------


//for Bitmap_word0(texel_h[15:0])
//-----------------------
//|Bits   |     Texel   |
//+-------+-------------+
//|1:0    | Texel[0][0] |
//|3:2    | Texel[0][1] |
//|5:4    | Texel[0][2] |
//|7:6    | Texel[0][3] |
//|9:8    | Texel[1][0] |
//|11:10  | Texel[1][1] |
//|13:12  | Texel[1][2] |
//|15:14  | Texel[1][3] |
//-----------------------

//for Bitmap_word1
//-----------------------
//|Bits   |     Texel   |
//+-------+-------------+
//|1:0    | Texel[2][0] |
//|3:2    | Texel[2][1] |
//|5:4    | Texel[2][2] |
//|7:6    | Texel[2][3] |
//|9:8    | Texel[3][0] |
//|11:10  | Texel[3][1] |
//|13:12  | Texel[3][2] |
//|15:14  | Texel[3][3] |
//-----------------------

//Code Encoding without transparcy
//00:   color0
//01:   color1
//10:   2/3 color_0 + 1/3 color_1
//11:   1/3 color_0 + 2/3 color_1



wire  [63:0]   texel = l1_dout;

wire  [31:0]   texel_h = texel[63:32];
wire  [31:0]   texel_l = texel[31:0];

wire          mask;
wire          transprancy;

// stage 2

wire [15:0]   base_color1 = texel_l[31:16];
wire [15:0]   base_color0 = texel_l[15:0];

wire [7:0]    base_color1_r = {base_color0[15:11],base_color0[15:13]};
wire [7:0]    base_color1_g = {base_color0[10:5] ,base_color0[10:9] };
wire [7:0]    base_color1_b = {base_color0[4:0]  ,base_color0[4:2]  };
wire [7:0]    base_color2_r = {base_color1[15:11],base_color1[15:13]};
wire [7:0]    base_color2_g = {base_color1[10:5] ,base_color1[10:9] };
wire [7:0]    base_color2_b = {base_color1[4:0]  ,base_color1[4:2]  };

wire [9:0]    temp_color1_r = {base_color1_r,1'b0} + {1'b0,base_color2_r} + 1'b1;
wire [9:0]    temp_color1_g = {base_color1_g,1'b0} + {1'b0,base_color2_g} + 1'b1;
wire [9:0]    temp_color1_b = {base_color1_b,1'b0} + {1'b0,base_color2_b} + 1'b1;
      
wire [9:0]    temp_color2_r = {base_color2_r,1'b0} + {1'b0,base_color1_r} + 1'b1;
wire [9:0]    temp_color2_g = {base_color2_g,1'b0} + {1'b0,base_color1_g} + 1'b1;
wire [9:0]    temp_color2_b = {base_color2_b,1'b0} + {1'b0,base_color1_b} + 1'b1;

//wire [7:0]    inter_color1_r = ({temp_color1_r,2'b0} + temp_color1_r + base_color1_r)>>4;//(temp_color1_r>>6) +(temp_color1_r>>4);// + (temp_color1_r>>2);
//wire [7:0]    inter_color1_g = ({temp_color1_g,2'b0} + temp_color1_g + base_color1_g)>>4;//(temp_color1_g>>6) +(temp_color1_g>>4);// + (temp_color1_g>>2);
//wire [7:0]    inter_color1_b = ({temp_color1_b,2'b0} + temp_color1_b + base_color1_b)>>4;//(temp_color1_b>>6) +(temp_color1_b>>4);// + (temp_color1_b>>2);                                                              
//wire [7:0]    inter_color2_r = ({temp_color2_r,2'b0} + temp_color2_r + base_color2_r)>>4;//(temp_color2_r>>6) +(temp_color2_r>>4);// + (temp_color2_r>>2);
//wire [7:0]    inter_color2_g = ({temp_color2_g,2'b0} + temp_color2_g + base_color2_g)>>4;//(temp_color2_g>>6) +(temp_color2_g>>4);// + (temp_color2_g>>2);
//wire [7:0]    inter_color2_b = ({temp_color2_b,2'b0} + temp_color2_b + base_color2_b)>>4;//(temp_color2_b>>6) +(temp_color2_b>>4);// + (temp_color2_b>>2);

wire [7:0]    inter_color1_r = (base_color1_r*3 + base_color2_r)>>2;
wire [7:0]    inter_color1_g = (base_color1_g*3 + base_color2_g)>>2;
wire [7:0]    inter_color1_b = (base_color1_b*3 + base_color2_b)>>2;                                                            
wire [7:0]    inter_color2_r = (base_color2_r*3 + base_color1_r)>>2;
wire [7:0]    inter_color2_g = (base_color2_g*3 + base_color1_g)>>2;
wire [7:0]    inter_color2_b = (base_color2_b*3 + base_color1_b)>>2;


wire [7:0]    inter_color3_r = ({1'b0,base_color1_r} + {1'b0,base_color2_r})>>1;
wire [7:0]    inter_color3_g = ({1'b0,base_color1_g} + {1'b0,base_color2_g})>>1;
wire [7:0]    inter_color3_b = ({1'b0,base_color1_b} + {1'b0,base_color2_b})>>1;

/*
wire [13:0]    inter_color1_r_tmp;
wire [13:0]    inter_color1_g_tmp;
wire [13:0]    inter_color1_b_tmp;
wire [13:0]    inter_color2_r_tmp;
wire [13:0]    inter_color2_g_tmp;
wire [13:0]    inter_color2_b_tmp;


mab mab1r(8'd3, 8'd5, base_color1_r, base_color2_r, inter_color1_r_tmp);
mab mab1g(8'd3, 8'd5, base_color1_g, base_color2_g, inter_color1_g_tmp);
mab mab1b(8'd3, 8'd5, base_color1_b, base_color2_b, inter_color1_b_tmp);
mab mab2r(8'd3, 8'd5, base_color2_r, base_color1_r, inter_color2_r_tmp);
mab mab2g(8'd3, 8'd5, base_color2_g, base_color1_g, inter_color2_g_tmp);
mab mab2b(8'd3, 8'd5, base_color2_b, base_color1_b, inter_color2_b_tmp);
*/
//assign inter_color1_r = inter_color1_r_tmp >> 3;
//assign inter_color1_g = inter_color1_g_tmp >> 3;
//assign inter_color1_b = inter_color1_b_tmp >> 3;
//assign inter_color2_r = inter_color2_r_tmp >> 3;
//assign inter_color2_g = inter_color2_g_tmp >> 3;
//assign inter_color2_b = inter_color2_b_tmp >> 3;



wire [23:0]   wire_color1 =   {base_color1_r, base_color1_g, base_color1_b};    // color1 with/without transprancy
wire [23:0]   wire_color2 =   {base_color2_r, base_color2_g, base_color2_b};    // color2 with/without transprancy
wire [23:0]   wire_color3 =   {inter_color1_r,inter_color1_g,inter_color1_b}; // color3 without transprancy
//wire [23:0]   wire_color3_2 = {inter_color3_r,inter_color3_g,inter_color3_b}; // color3 with transprancy
//wire [23:0]   wire_color3 = transprancy ? wire_color3_2 : wire_color3_1;
wire [23:0]   wire_color4 =   {inter_color2_r,inter_color2_g,inter_color2_b}; // color4 with/without transprancy

reg [23:0]   color1;
reg [23:0]   color2;
reg [23:0]   color3;
reg [23:0]   color4;

reg  [7:0]   pixel_choose_inter;
reg  [1:0]   pixel_choose;


assign    mask = (base_color0 > base_color1) ? 1'b1: 1'b0;  //1: disbale transprancy, which means mask.
assign    transprancy = ~mask;





always@(*)
begin
        case(in_texture_addr_ff2[3:2])
                2'b00: pixel_choose_inter = texel_h[7:0];
                2'b01: pixel_choose_inter = texel_h[15:8];
                2'b10: pixel_choose_inter = texel_h[23:16];
                2'b11: pixel_choose_inter = texel_h[31:24];
        endcase
end




always@(posedge clk or negedge rst_n)
begin
        if(~rst_n) pixel_choose <=  0;
        else if (rd_en_ff2 && !cache_busy)
        begin
                case(in_texture_addr_ff2[1:0])
                        2'b00: pixel_choose <=  pixel_choose_inter[1:0];
                        2'b01: pixel_choose <=  pixel_choose_inter[3:2];
                        2'b10: pixel_choose <=  pixel_choose_inter[5:4];
                        2'b11: pixel_choose <=  pixel_choose_inter[7:6];
                endcase
        end
end


always @(posedge clk or negedge rst_n)
begin
        if(~rst_n) 
        begin
                color1 <=  0;
                color2 <=  0;
                color3 <=  0;
                color4 <=  0;
        end
        else if (rd_en_ff2 && !cache_busy)
        begin
                color1 <=  wire_color1;
                color2 <=  wire_color2;
                color3 <=  wire_color3;
                color4 <=  wire_color4;
        end
end


always@(*)
begin
        case(pixel_choose)
                2'b00: dxt_texel = color1;
                2'b01: dxt_texel = color2;
                2'b10: dxt_texel = color3;
                2'b11: dxt_texel = color4;
        endcase
end


endmodule
